# NoSQL Benchmark

API demo use for NoSQL Benchmarking. This project implements a DB factory that routes the API to the specific instance.

For this demo, I used Docker containers (which are awesome by the way) with only one server per product. The servers are running with
its original settings, I did not try to do further optimization or add extra servers to turn them into a cluster. However, feel free to do any 
change you want to test your own specific use case; just don't forget to modify the `config.json` file and/or the docker compose file.

For now, I am only supporting insert and find by id operations. Delete, update and find with filters are work in progress. I will also add Swagger to document the APIs as soon as I can.

Inside the `/resources` folder, you will find the Docker image to spawn the application, I still need to modify it to take an Env variable to specify the DB as part of the setup. 
You can also use the *dev* Docker version that is located under `/resources/dev`.

To simulate a client, I used JMeter. I will include my testing script in a few.

In order to provide better results, this project uses NodeJS `console.time` function to know exactly how long is each DB driver taking to process a request and not only the whole 
client-server response. The reason is very simple, frameworks like Express may add some overhead in the process and that may report misleading readings when you just monitor 
the response time between the client and the application server. Of course, you can always monitor app-server to db-server connection too but, again, that may hide some pre and 
post processing in the driver or SDK.

## Installation
The project requires NodeJS 6.x

```bash
npm install
```

### Run it
**In a container**
```sh
docker run -e DB=<couchbase, cassandra or mongodb> elfido/nosqlbenchmark:latest
```

**Without containers**
```sh
node index db=<db>
```
Where < db > can take any of the following values: couchbase, cassandra or mongodb

**Create a Docker image**
```bash
npm run docker
```

## Information
Tested with:

* Couchbase
	* Server Version Enterprise 4.5
	* Driver Version 2.1.6
* MongoDB
    * Server Version 3.3.6
    * Driver Version 2.1
* Cassandra
	* Server Version 3.5
	* Driver Version 3.0.2


### Couchbase setup

Create a bucket called "nosqlbenchmark" with no password.

Alternitevely, you can use a docker container using the official Couchbase Image:

```sh
docker run -d --name db -p 8091-8094:8091-8094 -p 11210:11210 couchbase
```

The administrator user of the container is ```admin``` and the password ```benchmark123```.

### MongoDB setup
```javascript
db.createUser({user: "nosql", pwd: "rules!", roles: [{role:"userAdminAnyDatabase", db: "admin"}]});
```

Then create a collection called "nosqlbenchmark".

Alternitevely, you can use a docker container using the official Couchbase Image:

```sh
$ docker run --name some-app --link some-mongo:mongo -d application-that-uses-mongo
```

### Cassandra setup

Create a keystore called "nosqlbenchmark". The application will create the required table as part of the DB connection when the application start.

## Testing API
```bash
curl -X POST http://localhost:3000/api/movie
```