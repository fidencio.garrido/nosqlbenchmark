'use strict';

var mongoClient = require("mongodb").MongoClient,
    ObjectID = require('mongodb').ObjectID,
    assert = require("assert");

var MongoDB = class MongoDB{
    
    constructor( settings ){
        this.collection = null;
        this.setupConnection( settings );
        this.db = null;
    }
    
    setupConnection( settings ){
        var _this = this;
        console.info("Trying to connect to server", settings.server);
        mongoClient.connect(settings.server, function(err, db) {
            assert.equal(null, err);
            console.info("Connected succesfully to server");
            _this.db = db;
            _this.collection = _this.db.collection("movies");
            //_this.db.close();
        });
    }
    
    update( item ){
        console.time("mongo:update");
        var _this = this,
            id = item._id;
        delete item._id;
        return new Promise(function(resolve, reject) {
            _this.collection.update({"_id": new ObjectID(id)},item, function(err, result){
                console.timeEnd("mongo:update");
                if (!err){
                    item._id = id;
                    resolve( item );
                } else{
                    reject( err );
                }
            });
        });
    }
    
    destroy( itemId ){
        console.time("mongo:destroy");
        var _this = this;
        return new Promise(function(resolve, reject){
            _this.collection.remove({"_id": new ObjectID(itemId)}, function(err, item){
                console.timeEnd("mongo:destroy");
                if(!err){
                    resolve(item);
                } else{
                    console.dir(err);
                    reject(err);
                }
            });
            console.timeEnd("mongo:destroy");
        });
    }
    
    findOne( itemId ){
        console.time("mongo:findOne");
        var _this = this;
        return new Promise(function(resolve, reject){
            _this.collection.findOne({"_id": new ObjectID(itemId)}, function(err, item){
                console.timeEnd("mongo:findOne");
                if(!err){
                    resolve(item);
                } else{
                    console.dir(err);
                    reject(err);
                }
            });
        });
    }
    
    createItem(item){
        console.time("mongo:insert");
        var _this = this;
        return new Promise(function(resolve, reject) {
            _this.collection.insertOne(item, function(err, result){
                console.timeEnd("mongo:insert");
                if (!err){
                    var id = new ObjectID(result.ops[0]._id),
                        itemInfo = {
                            id: id.toHexString()
                        };
                    resolve( itemInfo );
                } else{
                    reject( err );
                }
            });
        });
    }
};

module.exports = MongoDB;