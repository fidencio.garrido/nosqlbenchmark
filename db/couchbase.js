"use strict";

const couchbase  = require("couchbase"),
    uuid = require("node-uuid"),
    Nq = couchbase.N1qlQuery,
    maxRecheck = 10,
    intervalTime = 10000;
    
var recheckInterval = null;

const connection = {
	errorCount: 0,
	host: null,

	buckets: {},
	connInfo: {},

	_connection: null,
	
	getConnection: function(settings) {
		connection.settings = settings;
		return new Promise( (resolve, reject)=>{
			
			var cluster = new couchbase.Cluster(settings.server, ()=>{
			});
			connection._connection = cluster;
			connection.connInfo.connection = cluster;
			connection.connInfo.bucket = cluster.openBucket(settings.bucket, (err)=>{
				if (err){
					console.error("Failed to connect to cluster: " + err);
			        if (recheckInterval === null){
			        	console.log(`Retrying every ${intervalTime/1000} seconds`);
			        	recheckInterval = setInterval( ()=>{
			        		connection.errorCount++;
			        		console.log(`Attempt ${connection.errorCount}`);
			        		connection.getConnection(connection.settings);
			        		if( connection.errorCount >= maxRecheck ){
			        			clearInterval(recheckInterval);
			        			throw "Couchbase connection problem";
			        		}
			        	}, intervalTime);
			        }	
				} else{
					if (recheckInterval !== null){
						clearInterval(recheckInterval);
					}
					console.log("Connected to bucket");
					resolve(connection.connInfo);
				}
			});
		});
	},

	getBucket: function (name) {
		var _this = connection;

		if (_this.buckets[name]) {
			return _this.buckets[name];
		}

		var bucket = _this._connection.openBucket(name, function(err){
			if (err) {
		        console.error("Failed to connect to cluster: " + err);
		        if (recheckInterval === null){
		        	recheckInterval = setInterval( ()=>{
		        		connection.getConnection(connection.host);
		        	}, 3000);
		        }
		    }
		});

		_this.buckets[name] = bucket;
		return bucket;
	},
	
	connect(settings){
		return new Promise( (resolve, reject)=>{
			connection.getConnection(settings).then( (connInfo)=>{
				resolve(connInfo);
			}).catch( (err)=>{
				reject(err);
			});
		});
	}
};

var toBool = {
	'true': true,
	'false': false
};

function convertValue(value) {
	var val = parseInt(value);

	if (!isNaN(val)) {
		return val;
	}

	if (toBool.hasOwnProperty(value)) {
		return toBool[value];
	}

	return '"' + value + '"';
}

var Couchbase = class Couchbase {

	connect(settings){
		let _this = this;
		connection.connect(settings).then( (connInfo)=>{
			_this.connection = connection._connection;
			_this.bucketName = settings.bucket;
			_this.bucket = connection.connInfo.bucket;
			_this.bucket.enableN1ql(settings.queryServer);
			_this.docType = settings.docType;
		}).then( (err)=>{
			throw("Problem connecting to Couchbase");
		});
	}

	constructor(settings) {
		this.connect(settings);
	}

   findOne(id) {
   	  console.time("cb:findOne");
      var _this = this;
      return new Promise(function(resolve, reject) {
        connection.connInfo.bucket.get(id, function(err, result) {
           console.timeEnd("cb:findOne");
           if (err) {
              reject(err);
           } else {
           	  result.value.id = id;
           	  resolve(result.value);
           }
        });
      });
   }

   createItem(item) {
      var _this = this,
          id = uuid.v1();

      console.time("cb:insert");
      return new Promise(function(resolve, reject) {
        connection.connInfo.bucket.insert(id, item, function(err, result) {
           console.timeEnd("cb:insert");
           if (err) {
              reject(err);
           } else {
           	  var itemInfo = {
           	  	     id: id
           	  	  };
           	  resolve(itemInfo);
           }
        });
      });
   }

   update(id, item) {
   	  console.time("cb:update");
   	  var _this = this;

      return new Promise(function(resolve, reject) {
        connection.connInfo.bucket.replace(id, item, function(err, result) {
           console.timeEnd("cb:update");
           if (err) {
              reject(err);
           } else {
           	  var itemInfo = {
           	  	     id: id
           	  	  };
           	  resolve(itemInfo);
           }
        });
      });    
   }

   findAll(where) {
   	  var _this = this,
   	  	  selectClause = 'SELECT * ',
   	  	  fromClause = 'FROM `' + this.bucketName + '`',
   	  	  whereClause = '',
   	  	  query;

   	  Object.keys(where).forEach(function(key, i) {
   	     if (i === 0) {
   	     	whereClause = 'WHERE ';
   	  	 } else {
   	  	 	whereClause += ' AND ';
   	  	 }

   	  	 whereClause += key + ' = ' + convertValue(where[key]);
   	  });

   	  query = selectClause + fromClause + whereClause;
   	  let n1qlquery = Nq.fromString(query);
   	  
   	  console.time("cb:findAll");
      return new Promise(function(resolve, reject) {
        connection.connInfo.bucket.query(n1qlquery, function(err, results) {
           console.timeEnd("cb:findAll");
           if (err) {
              reject(err);
           } else {
           	  resolve(results);
           }
        });
      });
   }

};

module.exports = Couchbase;
