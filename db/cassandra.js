'use strict';
var cassandra = require('cassandra-driver'),
    client = null,
    assert = require("assert");

var Cassandra = class Cassandra{
    constructor( settings ){
        this.keyspace = settings.keyspace;
        this.setupConnection( settings );
    }

    createKeySpace(ks){
        // client.execute("DROP TABLE IF EXISTS nosqlbenchmark.movies;", function(){

        // });
        client.execute("CREATE KEYSPACE IF NOT EXISTS nosqlbenchmark WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };",
            function(){
                console.info("Keyspace created");
            });
        client.execute("CREATE TABLE IF NOT EXISTS nosqlbenchmark.movies (id uuid PRIMARY KEY, title text, format text, genre text, language text, actors set<text>, release_year int, imdb_link text, raitings map<text,text>, availability set<text>);",
            function(err){
                assert.equal(null, err);
                console.info("Table created");
            });
    }

    setupConnection( settings ){
        var _this = this;
        client = new cassandra.Client({ contactPoints: settings.servers, keyspace: settings.keypsace});
        client.connect(function(err){
            assert.equal(null, err);
            console.info("Connected succesfully to server");
            _this.createKeySpace(settings.keypsace);
        });
    }

    createItem(item){
        console.time("cassandra:insert");
        var _this = this;
        return new Promise(function(resolve, reject) {
            item.id = cassandra.types.uuid();
            var insertStatement = "INSERT INTO "+_this.keyspace+".movies JSON '" + JSON.stringify(item) + "'";
             client.execute(insertStatement,
                function(err,res){
                    console.timeEnd("cassandra:insert");
                    if(!err){
                        var itemInfo = {
                            id: item.id
                        };
                        resolve(itemInfo);
                    } else {
                        reject(err);
                    }
                });
        });
    }

    findOne( itemId ){
        console.time("cassandra:findOne");
        var _this = this;
        return new Promise(function(resolve, reject){
            var cqlStatement = 'SELECT * FROM ' + _this.keyspace + '.movies WHERE id = ?;';
            client.execute(cqlStatement, [ itemId ], function(err, item) {
                console.timeEnd("cassandra:findOne");
                if (err) {
                    reject(err);
                } else {
                    resolve(item.rows[0]);
                }
            });
        });
    }
};

module.exports = Cassandra;
