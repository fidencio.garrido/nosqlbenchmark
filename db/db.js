"use strict";

const config = require("../services/config.js"),
    cassandra = require("./cassandra.js"),
    couchbase = require("./couchbase.js"),
    mongodb = require("./mongodb.js");


var DBFactory = class DBFactory{
    static getDB(){
        let argDb = (typeof config.getProcessArgs("db") === "string") ? config.getProcessArgs("db") : null,
            db = argDb  ||  config.getConfiguration().db,
            DB;
        console.info("DB Engine: " + db);
        switch( db ){
            case "cassandra":
                DB = new cassandra( config.getConfiguration().cassandra );
                break;
            case "couchbase":
                DB = new couchbase( config.getConfiguration().couchbase );
                break;
            case "mongodb":
                DB = new mongodb( config.getConfiguration().mongodb );
                break;
            default:
                DB = null;
                break;
        }
        return DB;
    }
};

module.exports = DBFactory;