var port = process.env.APP_PORT || 3000,
    express = require("express"),
    morgan = require("morgan"),
    bodyParser = require("body-parser"),
    healthRoutes = require("./routes/health"),
    moviesRoutes = require("./routes/movies"),
    cluster = require('cluster'),
    numCPUs = require('os').cpus().length,
    app = express();




var App = { 
    api:{
      movie: "/api/movie"
    },
    initExpress: function(){
        //app.use( morgan('combined') );
        app.use( bodyParser.json() );
        app.get( "/api/ping", healthRoutes.getPingResponse );
        app.get( this.api.movie + "/:id", moviesRoutes.findOne) ;
        app.get( this.api.movie, moviesRoutes.findAll );
        app.post( this.api.movie, moviesRoutes.create );
        app.put( this.api.movie, moviesRoutes.update );
        app.delete( this.api.movie + "/:id", moviesRoutes.destroy );
    },
    init: function(){
        if (cluster.isMaster){
            console.info("Starting master node");
            for (var i=0; i<numCPUs; i++){
                cluster.fork();
            }
        } else{
            this.initExpress();
            console.info("Starting secondary node");
            app.listen( port, function(){
               console.info("Starting app on port ", port); 
            });
        }
        
    }
};

App.init();