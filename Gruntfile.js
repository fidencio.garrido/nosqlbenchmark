module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		eslint: {
			options:{
				configFile: "resources/eslint.json"	
			},
			target: ["index.js", "db/**/*.js", "routes/**/*.js", "services/**/.js"]
		}
	});

	grunt.loadNpmTasks("grunt-eslint");

	grunt.registerTask('default', ['eslint']);
};