#!/bin/bash
clear
echo $1
if [ -z $1 ]; then
    echo This command requires at least the first of the following arguments:
    echo -- absolute path to a mapped volume
    echo -- port to expose \(optional\)
else
    if [ -z $2 ]; then
        PORT="-P" 
    else
        PORT="-p $2:3000" 
    fi
    echo Creating docker container and mapping to $1 and exposing using configuration for port $PORT
    # docker run --rm -it -v $1:/opt/app $PORT --link nosqlcassandra:cassandra-server --link nosqlcouchbase:couchbase-server --link nosqlmongo:mongo-server -it nosqlbenchmark-dev /bin/bash
    docker run --rm -it -v $1:/opt/app $PORT --link cb-benchmark:couchbase-server -it nosqlbenchmark-dev /bin/bash
fi
