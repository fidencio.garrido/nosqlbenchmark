#!/bin/bash
echo Packing app
npm pack && mv nosqlbenchmark-0.2.0.tgz ./resources
echo Building Docker Image
docker build -t=elfido/nosqlbenchmark:0.2.0 -t=elfido/nosqlbenchmark:latest ./resources
echo Removing tar file
rm ./resources/nosqlbenchmark-0.2.0.tgz
echo Finished!