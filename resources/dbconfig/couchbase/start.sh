echo Configuring couchbase, this will take at least 15 seconds
sleep 15
couchbase-cli cluster-init -c couchbase-server:8091 -u admin -p benchmark123 --services data,index,query,fts --cluster-ramsize=500 --cluster-index-ramsize=400 --index-storage-setting=default --wait
echo Creating bucket
sleep 3
couchbase-cli bucket-create -c couchbase-server:8091 -u admin -p benchmark123 --bucket=nosqlbenchmark --bucket-type=couchbase --bucket-ramsize=300 --bucket-replica=1 --wait
echo Creating indexes
sleep 2
/opt/couchbase/bin/cbq -e couchbase://couchbase-server -u admin -p benchmark123 --script="create primary index id on nosqlbenchmark using gsi;"