var MovieService = require("../services/movies.js"),
    Movie = new MovieService();

var Movies = {
  create: function(req, res){
    var movieItem = req.body;
    Movie.create( movieItem ).then(function(item){
      res.send( item );
    }).catch(function(error){
      res.status(500).send("Server Error:" + error);
    });
  },
  update: function(req, res){
    var movieItem = req.body;
    Movie.update( movieItem ).then(function(item){
      res.send( item );
    }).catch(function(error){
      res.status(500).send("Server Error" + JSON.stringify(error));
    });
  },
  destroy: function(req, res){
    var movieId = req.params.id;
    Movie.destroy( movieId ).then( function(){
      res.status(200).send( {} );
    }).catch( function(error){
      console.dir(error);
      res.status(500).send("Item not found");
    });
  },
  findOne: function(req, res){
    var movieId = req.params.id;
    Movie.findOne( movieId ).then( function(movieItem){
      res.send( movieItem );
    }).catch( function(error){
      res.status(404).send("Item not found "+error);
    });
  },
  findAll: function(req, res){
    Movie.findAll({}).then( (movies)=>{
      res.send( movies );
    }).catch( (error)=>{
      res.status(500).send("There is a problem with the Database: " + error);
    });
  }
};

module.exports = Movies;