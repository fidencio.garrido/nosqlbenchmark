var fs = require("fs");

var Config = {
    getProcessArgs: function( token ){
        var args = (process.argv) ? process.argv : null;
        if (args!==null && typeof token === "string" && token.length>0){
            for(var arg in args){
                var param = args[arg],
                    lookingFor = token + "=";
                if ( param.indexOf( lookingFor ) === 0){
                    return param.replace(lookingFor, "");
                }
            }
        }
        return args;
    },
    getConfiguration: function(){
        var config = fs.readFileSync("./config.json", "utf8");
        return JSON.parse(config);
    }
};

module.exports = Config;