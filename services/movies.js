'use strict';

var DBFactory = require("../db/db.js"),
    DB;

var Movies = class Movies{
  
  constructor(){
    DB = DBFactory.getDB();
  }
  
  create(movie) {
    var _this = this;
    return new Promise(function(resolve, reject){
      if (typeof movie !== "object" || typeof movie === null){
        reject("Invalid document");
      }
      DB.createItem( movie ).then( function(serviceResponse){
        resolve( serviceResponse );
      } ).catch( function(error){
        reject(error);
      } );
    });
  }
  
  
  update(movie) {
    var _this = this;
    return new Promise(function(resolve, reject){
      DB.update( movie ).then( function(serviceResponse){
        resolve( serviceResponse );
      } ).catch( function(error){
        reject(error);
      } );
    });
  }
  
  destroy(movieId) {
    var _this = this;
    return new Promise(function(resolve, reject){
      DB.destroy(movieId).then(function(){
        resolve();
      }).catch(function(error){
        reject(error);
      });
    });
  }
  
  findOne(movieId) {
    var _this = this;
    return new Promise(function(resolve, reject){
      DB.findOne(movieId).then(function(serviceResponse){
        resolve(serviceResponse);
      }).catch(function(error){
        reject(error);
      });
    });
  }
  
  findAll(filters){
    return new Promise( (resolve, reject)=>{
      DB.findAll({}).then( (serviceResponse)=>{
        resolve(serviceResponse);
      }).catch( (error)=>{
        reject(error);
      });
    });
  }
      
};

module.exports = Movies;